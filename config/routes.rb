Rails.application.routes.draw do
  get '/auth/:provider/callback', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  resources :home, only: :index

  resources :projects
  resources :tasks, except: :update do
    collection do
      put :update
    end
    member do
      post :resume
    end
  end

  root 'home#index'
end
