class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.references :project, index: true, foreign_key: true
      t.integer :time
      t.string :name
      t.string :status

      t.timestamps null: false
    end
  end
end
