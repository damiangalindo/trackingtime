class AddTimeFieldsToTask < ActiveRecord::Migration
  def change
    add_column :tasks, :from_time, :time
    add_column :tasks, :to_time, :time
    add_column :tasks, :date_done, :date
  end
end
