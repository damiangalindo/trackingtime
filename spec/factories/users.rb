FactoryGirl.define do
  factory :user do
    provider "MyString"
    uid "MyString"
    name "MyString"
    token "MyString"
    secret "MyString"
    profile_image "MyString"
  end
end
