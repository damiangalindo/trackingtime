require 'rails_helper'

RSpec.describe SessionsController, type: :controller do
  context 'GET create' do
    describe 'when omniauth.auth' do
      before do
        request.env['omniauth.auth'] = OpenStruct.new({
          :provider => "twitter",
          :uid => "123456",
          :info => OpenStruct.new({
            :nickname => "johnqpublic",
            :name => "John Q Public",
            :image => "http://si0.twimg.com/sticky/default_profile_images/default_profile_2_normal.png"
          }),
          :credentials => OpenStruct.new({
            :token => "a1b2c3d4",
            :secret => "abcdef1234"
          })
        })
      end

      it 'creates a new user' do
        expect { get :create, provider: 'twitter' }.to change{ User.count }.by(1)
      end
    end
  end

  context 'DELETE destroy' do
    describe 'when current_user' do
      before do
        session[:user_id] = user.id
      end

      let(:user) { create(:user) }

      it 'deletes the current user_id session variable' do
        delete :destroy

        expect(session[:user_id]).to be_nil
      end
    end
  end
end
