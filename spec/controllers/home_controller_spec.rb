require 'rails_helper'

RSpec.describe HomeController, type: :controller do
  context 'GET index' do
    context 'with current_user' do
      before do
        session[:user_id] = user.id
      end
      let!(:user) { create(:user) }
      subject { get :index, session: { user_id: user.id } }

      it 'redirects to the projects index' do
        expect(subject).to redirect_to(projects_path)
      end
    end

    context 'without current_user' do
      it 'shows the home view' do
        get :index
        expect(response.code).to eq('200')
      end
    end
  end
end
