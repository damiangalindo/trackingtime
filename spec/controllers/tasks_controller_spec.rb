require 'rails_helper'

RSpec.describe TasksController, type: :controller do
  describe 'POST create' do
    describe 'when logged in' do
      before do
        login_in_user(user)
      end

      let(:user) { create(:user) }
      let(:project) { create(:project, user: user) }

      context 'without project_id' do
        let(:params) do
          {
            task: {
              project_id: '',
              name: 'Task Test',
              description: 'Task Description'
            }
          }
        end

        it 'creates a new task' do
          expect { xhr :post, :create, params }.to change{ Task.count }.by 1
        end

        it 'decorates the task' do
          xhr :post, :create, params
          expect(assigns(:task)).to be_decorated
        end
      end

      context 'with project_id' do
        let(:params) do
          {
            task: {
              project_id: project.id,
              name: 'Task Test',
              description: 'Task Description'
            }
          }
        end

        let(:user) { create(:user) }
        let(:project) { create(:project, user: user) }

        it 'creates a new task' do
          expect { xhr :post, :create, params }.to change{ Task.count }.by 1
        end

        it 'decorates the task' do
          xhr :post, :create, params
          expect(assigns(:task)).to be_decorated
        end
      end
    end

    describe 'when logged out' do
      subject { post :create }
      it 'redirects user to root_path' do
        expect(subject).to redirect_to(root_path)
      end
    end
  end

  describe 'PUT update' do
    describe 'when logged in' do
      before do
        login_in_user(user)
      end

      let(:user) { create(:user) }
      let(:project) { create(:project, user: user) }

      context 'for user task' do
        before do
          xhr :put, :update, params_user_task
        end

        let!(:user_task) { create(:task, user: user) }

        let(:params_user_task) do
          {
            task: {
              id: user_task.id,
              time: 10
            }
          }
        end

        it 'updates the time for a task' do
          user_task.reload
          expect(user_task.time).to eq(10)
        end

        it 'decorates the task' do
          expect(assigns(:task)).to be_decorated
        end
      end

      context 'for project task' do
        before do
          xhr :put, :update, params_project_task
        end

        let!(:project_task) { create(:task, project: project) }

        let(:params_project_task) do
          {
            task: {
              id: project_task.id,
              time: 20
            }
          }
        end

        it 'updates the time for a task' do
          project_task.reload
          expect(project_task.time).to eq(20)
        end

        it 'decorates the task' do
          expect(assigns(:task)).to be_decorated
        end
      end

      context 'when missing time' do
        let!(:project_task) { create(:task, project: project) }

        let(:params_project_task) do
          {
            task: {
              id: project_task.id,
              time: ''
            }
          }
        end

        it 'does not update the time for a task' do
          xhr :put, :update, params_project_task
          project_task.reload
          expect(project_task.time).to eq(nil)
        end

        it 'returns an error' do
          xhr :put, :update, params_project_task
          expect(assigns(:task).errors.full_messages_for(:time)).to include("Time can't be blank")
        end
      end
    end

    describe 'when logged out' do
      subject { put :update }
      it 'redirects user to root_path' do
        expect(subject).to redirect_to(root_path)
      end
    end
  end

  describe 'DELETE destroy' do
    describe 'when logged in' do
      before do
        login_in_user(user)
      end

      let(:user) { create(:user) }
      let(:project) { create(:project, user: user) }
      let!(:task) { create(:task, project: project) }

      it 'destroys a task' do
        expect { xhr :delete, :destroy, id: task.id }.to change(Task, :count).by(-1)
      end
    end

    describe 'when logged out' do
      subject { delete :destroy, id: 1 }
      it 'redirects user to root_path' do
        expect(subject).to redirect_to(root_path)
      end
    end
  end

  describe 'POST resume' do
    describe 'when logged in' do
      before do
        login_in_user(user)
      end

      let(:user) { create(:user) }
      let(:project) { create(:project, user: user) }

      context 'for user task' do
        before do
          xhr :post, :resume, params_user_task
        end

        let!(:user_task) { create(:task, user: user) }

        let(:params_user_task) do
          {
            id: user_task.id
          }
        end

        it 'resumes a task creating a new one with the data of the resumed' do
          expect(assigns(:task).name).to eq(user_task.name)
        end
      end

      context 'for project task' do
        before do
          xhr :post, :resume, params_project_task
        end

        let!(:project_task) { create(:task, project: project) }

        let(:params_project_task) do
          {
            id: project_task.id
          }
        end

        it 'resumes a task creating a new one with the data of the resumed' do
          expect(assigns(:task).name).to eq(project_task.name)
        end
      end

      context 'when missing time' do
        let!(:project_task) { create(:task, project: project) }

        let(:params_project_task) do
          {
            task: {
              id: project_task.id,
              time: ''
            }
          }
        end

        it 'does not update the time for a task' do
          xhr :put, :update, params_project_task
          project_task.reload
          expect(project_task.time).to eq(nil)
        end

        it 'returns an error' do
          xhr :put, :update, params_project_task
          expect(assigns(:task).errors.full_messages_for(:time)).to include("Time can't be blank")
        end
      end
    end

    describe 'when logged out' do
      subject { post :resume, id: 1 }
      it 'redirects user to root_path' do
        expect(subject).to redirect_to(root_path)
      end
    end
  end
end
