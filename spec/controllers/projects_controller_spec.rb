require 'rails_helper'

RSpec.describe ProjectsController, type: :controller do
  describe 'GET index' do
    describe 'when logged in' do
      before do
        login_in_user(user)

        get :index
      end

      let(:user) { create(:user) }
      let(:project) { create(:project, user: user) }
      let!(:user_task) { create(:task, user: user) }
      let!(:project_task) { create(:task, project: project) }

      %w[task tasks projects].each do |variable|
        it "sets the controller instance variable #{variable}" do
          expect(assigns(variable.to_sym)).not_to be_nil
        end
      end

      %w[tasks projects].each do |variable|
        it "decorates the #{variable}" do
          expect(assigns(variable.to_sym)).to be_decorated
        end
      end
    end

    describe 'when logged out' do
      subject { get :index }
      it 'redirects user to root_path' do
        expect(subject).to redirect_to(root_path)
      end
    end
  end

  describe 'GET new' do
    describe 'when logged in' do
      before do
        login_in_user(user)

        xhr :get, :new
      end

      let(:user) { create(:user) }

      it 'sets the controller instance variable project' do
        expect(assigns(:project)).not_to be_nil
      end
    end

    describe 'when logged out' do
      subject { get :new }
      it 'redirects user to root_path' do
        expect(subject).to redirect_to(root_path)
      end
    end
  end

  describe 'POST create' do
    describe 'when logged in' do
      before do
        login_in_user(user)
      end

      let(:user) { create(:user) }

      context 'with good params' do
        let(:good_params) do
          {
            project: {
              name: 'Project Test',
              description: 'Project Description'
            }
          }
        end

        it 'creates a new project' do
          expect { xhr :post, :create, good_params }.to change{ Project.count }.by 1
        end
      end

      context 'with bad params' do
        let(:bad_params) do
          {
            project: {
              name: '',
              description: 'Project Description'
            }
          }
        end

        it 'does not create a new project' do
          expect { xhr :post, :create, bad_params }.not_to change{ Project.count }
        end
      end
    end

    describe 'when logged out' do
      subject { post :create }
      it 'redirects user to root_path' do
        expect(subject).to redirect_to(root_path)
      end
    end
  end

  describe 'DELETE destroy' do
    describe 'when logged in' do
      before do
        login_in_user(user)
      end

      let(:user) { create(:user) }
      let!(:project) { create(:project, user: user) }

      it 'destroys a project' do
        expect { xhr :delete, :destroy, id: project.id }.to change(Project, :count).by(-1)
      end
    end

    describe 'when logged out' do
      subject { delete :destroy, id: 1 }
      it 'redirects user to root_path' do
        expect(subject).to redirect_to(root_path)
      end
    end
  end
end
