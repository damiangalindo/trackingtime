require 'rails_helper'

RSpec.describe Task, type: :model do
  context 'Validations' do
    it{ validate_presence_of(:time).on(:update)  }
  end

  context 'Relations' do
    it{ is_expected.to belong_to :user }
    it{ is_expected.to belong_to :project }
  end

  context 'Callbacks' do
    let(:user) { create(:user) }

    context 'set_date' do
      let(:task) { build(:task, user: user, from_time: Time.now, to_time: Time.now + 1.minute) }

      it 'sets the date' do
        task.save

        expect(task.date_done).to eq(Date.today)
      end
    end

    context 'set_time' do
      let(:task) { build(:task, user: user, from_time: Time.now, to_time: Time.now + 1.minute) }

      it 'sets the time' do
        task.save

        expect(task.time).to eq(60)
      end
    end

    context 'set_from_time' do
      let(:task) { create(:task, user: user, from_time: '') }

      before do
        allow(Time).to receive(:now).and_return(Time.parse('2017-08-21 23:59:27 UTC'))
        task
      end

      it 'sets from time' do
        expect(task.from_time).to eq('2017-08-21 23:59:27 UTC')
      end
    end
  end
end
