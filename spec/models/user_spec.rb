require 'rails_helper'

RSpec.describe User, type: :model do
  context 'Validations' do
    it{ validate_presence_of(:provider)  }
  end

  context 'Relations' do
    it{ is_expected.to have_many :projects }
    it{ is_expected.to have_many :tasks }
  end

  context 'Class Methods' do
    context '#find_or_create_from_auth_hash' do
      let!(:auth_hash) do
        OpenStruct.new({
          :provider => "twitter",
          :uid => "123456",
          :info => OpenStruct.new({
            :nickname => "johnqpublic",
            :name => "John Q Public",
            :image => "http://si0.twimg.com/sticky/default_profile_images/default_profile_2_normal.png"
          }),
          :credentials => OpenStruct.new({
            :token => "a1b2c3d4",
            :secret => "abcdef1234"
          })
        })
      end

      it 'creates a new user' do
        expect{ described_class.find_or_create_from_auth_hash(auth_hash) }.to change { User.count }.by 1
      end
    end
  end
end
