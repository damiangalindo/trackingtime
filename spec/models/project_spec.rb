require 'rails_helper'

RSpec.describe Project, type: :model do
  context 'Validations' do
    it{ validate_presence_of(:name)  }
  end

  context 'Relations' do
    it{ is_expected.to have_many(:tasks).dependent(:destroy) }
    it{ is_expected.to belong_to :user }
  end
end
