// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require popper
//= require bootstrap
//= require jquery-ui/widgets/datepicker
//= require jquery-ui-timepicker-addon
//= require_tree .

$(document).on('ready', function() {
  var timer = new Timer();

  $('.js-start-timer').on('click', function(e) {
    e.preventDefault();
    $form = $('#new_task');

    var time_val = $form.find('#task_from_time').val()

    if(time_val === ''){
      timer.start();
      timer.addEventListener('secondsUpdated', function (e) {
        $('#elapsed_time').html(timer.getTimeValues().toString());
      });
    }

    $form.submit();
  })

  $('.js-stop-timer').on('click', function(e) {
    e.preventDefault();
    var $form = $('#update_task');
    var time =  timer.getTimeValues().seconds;
    timer.stop();
    $('#elapsed_time').html('00:00:00');
    $form.find('#task_time').val(time);
    $form.submit();
  })

  $(document).on('click', '.js-task-resume', function(e){
    e.preventDefault();
    $elem = $(this);
    var task_id = $elem.data('taskId');

    $.post('/tasks/' + task_id + '/resume/').
      done(function(response) {
        var task_id = response['task_id'];

        $('.js-start-timer').hide();
        $('.js-stop-timer').show();

        $('#task_id').val(task_id);
      })

    timer.stop();

    timer.start();

    timer.addEventListener('secondsUpdated', function(e) {
      $('#elapsed_time').html(timer.getTimeValues().toString());
    });
  })

  $(document).on('click', '.js-new-task', function(e) {
    e.preventDefault();

    $elem = $(this);

    var project_id = $elem.data('projectId');

    $form = $('#new_task');

    $('#task_project_id').val(project_id)

    timer.start();
    timer.addEventListener('secondsUpdated', function(e) {
      $('#elapsed_time').html(timer.getTimeValues().toString());
    });

    $form.submit();
  })

  $(document).on('click', '.js-create-project', function(e) {
    e.preventDefault();

    $('#js-new-project-form').submit();
  })

  $(document).on('click', '.js-update-task', function(e) {
    e.preventDefault();

    $('#js-edit-task-form').submit();
  })

  $('.datepicker').datepicker({
    maxDate: "+0D"
  });

  $('#elapsed_time').on('click', function(e) {
    $('#task_done_from_to').toggle();
  })

  $.timepicker.timeRange(
    $('.time-from'),
    $('.time-to'),
    {
      minInterval: (1000*60),
  		timeFormat: 'HH:mm'
  	}
  );
})

$.fn.render_form_errors = function(model_name, errors) {
  console.log(errors)
  var form;
  form = this;
  this.clear_form_errors();
  return $.each(errors, function(field, messages) {
    var input;
    input = form.find('input, select, textarea').filter(function() {
      var name;
      name = $(this).attr('name');
      if (name) {
        return name.match(new RegExp(model_name + '\\[' + field + '\\(?'));
      }
    });
    input.closest('.form-group').addClass('is-invalid');
    return input.parent().append('<span class="help-block">' + $.map(messages, function(m) {
      return m.charAt(0).toUpperCase() + m.slice(1);
    }).join('<br />') + '</span>');
  });
};

$.fn.clear_form_errors = function() {
  this.find('.form-group').removeClass('is-invalid');
  return this.find('span.help-block').remove();
};
