class TasksController < ApplicationController
  before_action :build_task, only: [:new, :create]

  def edit
    @task = Task.find(params[:id])
    @projects = current_user.projects

    @task = @task.decorate

    respond_to :js
  end

  def create
    @task.attributes = task_params

    @task = @task.decorate if @task.save

    flash[:success] = 'Task created'

    respond_to :js
  end

  def update
    @task = Task.find(task_params[:id])

    @task.update_attributes(task_params)

    @task = @task.decorate

    respond_to :js
  end

  def destroy
    @task = Task.find(params[:id])
    @task.destroy

    flash[:success] = 'Task Deleted'

    respond_to :js
  end

  def resume
    @task = Task.find(params[:id])

    @task = @task.dup

    @task.time = nil
    @task.from_time = nil
    @task.to_time = nil

    @task.save

    render json: { task_id: @task.id }
  end

  private

  def build_task
    project_id = params[:project_id] || params.dig(:task, :project_id)

    @task = if project_id.present?
              project = Project.find(project_id)
              project.tasks.build
            else
              current_user.tasks.new
            end
  end

  def task_params
    params.require(:task).permit(:id, :name, :date_done, :from_time, :to_time, :time, :description, :project_id)
  end
end
