class ProjectsController < ApplicationController
  def index
    @task = Task.new
    @tasks = current_user.tasks_without_project.decorate
    @projects = current_user.projects.includes(:tasks).decorate
  end

  def new
    @project = current_user.projects.build
    respond_to :js
  end

  def create
    @project = current_user.projects.new(project_params)

    flash[:success] = 'Project created' if @project.save
    @project = @project.decorate

    respond_to :js
  end

  def destroy
    @project = Project.find(params[:id])
    @project.destroy

    flash[:success] = 'Project Deleted'

    respond_to :js
  end

  private

  def project_params
    params.require(:project).permit(:name)
  end
end
