class User < ActiveRecord::Base
  has_many :projects
  has_many :tasks

  validates :provider, presence: true

  def self.find_or_create_from_auth_hash(auth_hash)
    user = where(provider: auth_hash.provider, uid: auth_hash.uid).first_or_create
    user.update(
      name: auth_hash.info.nickname,
      profile_image: auth_hash.info.image,
      token: auth_hash.credentials.token,
      secret: auth_hash.credentials.secret,
      nickname: auth_hash.info.nickname
    )
    user
  end

  def tasks_without_project
    tasks.where('project_id is null')
  end
end
