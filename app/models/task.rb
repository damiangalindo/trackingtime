class Task < ActiveRecord::Base
  belongs_to :project
  belongs_to :user

  scope :default, -> { order(created_at: :desc) }
  scope :tasks_without_project, -> { includes(:tasks).where('tasks.project_id is null') }

  validates :time, presence: true, on: :update

  before_save :set_date
  before_save :set_time
  before_create :set_from_time
  before_update :set_to_time

  private

  def set_date
    if date_done.blank?
      self.date_done = Date.today
    end
  end

  def set_time
    if from_time && to_time
      elapsed = self.to_time - self.from_time
      self.time = Time.at(elapsed).utc
    end
  end

  def set_from_time
    if from_time.blank?
      self.from_time = Time.now
    end
  end

  def set_to_time
    if self.to_time.blank?
      self.to_time = Time.now
    end
  end
end
