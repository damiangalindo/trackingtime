class TaskDecorator < Draper::Decorator
  delegate_all

  def elapsed_time
    time = object.time || 0
    Time.at(time).utc.strftime("%H:%M:%S")
  end

  def project_name
    project = object.project
    project ? project.name : 'No project'
  end

  def from_to
    if object.from_time && object.to_time
      "#{object.from_time.utc.strftime("%H:%M %p")} - #{object.to_time.utc.strftime("%H:%M %p")}"
    end
  end
end

# TODO fix time elapsed
