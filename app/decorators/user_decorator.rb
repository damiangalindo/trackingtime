class UserDecorator < Draper::Decorator
  delegate_all
  decorates_association :tasks

  def tasks_count
    object.tasks.count
  end

end
